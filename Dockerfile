FROM alpine:3.10.0

LABEL maintainer="Andrew Aadland <aa23@protonmail.com>"
LABEL name="toolshed"
LABEL version="0.3.0"

RUN apk add --no-cache \
# network tools
  bind-tools curl nmap net-tools netcat-openbsd tcpdump ngrep \
# misc clients
  mysql-client redis \
# misc
  bash vim coreutils jq ca-certificates

# un-comment if you want to have a non-root user
# RUN addgroup -g 1000 -S noob && adduser -u 1000 -S noob -G noob

ENTRYPOINT ["/bin/bash"]
